
import React, { useState } from 'react';
import db from '../../Firebase/FirebaseConfig';
import './Form.scss'

const Formulario = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');

    const onSubmit = (e) => {
        e.preventDefault();

        db.collection('users').add({
            name: name,
            email: email
        }).then(() => {
            console.log('Se agregó correctamente');
        }).catch((error) => {
            console.log('Error, al intentar agregar el usuario');
            console.log(error)
        })

        setName('');
        setEmail('');
    }

    return (
        <form action="" className="form" onSubmit={ onSubmit }>
            <input
                type="text"
                className="form__input"
                name="name"
                value={ name }
                onChange={ (e) => setName(e.target.value) }
                placeholder="Nombre"
            />

            <input
                type="email"
                className="form__input"
                name="email"
                value={ email }
                onChange={ (e) => setEmail(e.target.value) }
                placeholder="Email"
            />

            <button type="submit" className="form__button">Agregar</button>
        </form>
    );
}

export default Formulario;