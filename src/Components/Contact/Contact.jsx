import React, { useState } from 'react';
import db from '../../Firebase/FirebaseConfig';
import './Contact.scss';

const Contact = ({ id, name, email }) => {
    const [edit, setEdit] = useState(false);
    const [newName, setNewName] = useState(name);
    const [newEmail, setNewEmail] = useState(email);

    const updateContact = (e) => {
        e.preventDefault();

        db.collection('users').doc(id).update({
            name: newName,
            email: newEmail
        }).then(() => {
            console.log('El usuario se actualizó correctamente')
        }).catch((error) => {
            console.log('Error al actualizar el usuario', error)
        })

        setEdit(false)
    }

    const deleteContact = (id) => {
        db.collection('users').doc(id).delete()
        .then(() => {
            console.log('El usuario se borró correctamente')
        }).catch((error) => {
            console.log('Error al borrar el usuario', error)
        })
    }

    return (
        <div className="container-contact">
            {edit 
            
            ?

                <form action="" onSubmit={updateContact}>
                    <input
                        type="text"
                        className="container-contact__input"
                        name="name"
                        value={newName}
                        onChange={(e) => {setNewName(e.target.value)}}
                        placeholder="Name"
                    />

                    <input
                        type="text"
                        className="container-contact__input"
                        name="email"
                        value={newEmail}
                        onChange={(e) => {setNewEmail(e.target.value)}}
                        placeholder="Email"
                    />

                    <button
                        type="submit"
                        className="container-contact__button"
                    >
                        Actualizar
                    </button>
                </form>

                : 

                    <>
                        <div className="container-contact__name">{name}</div>
                        <div className="container-contact__email">{email}</div>
                        <button onClick={() => setEdit(!edit)} className="container-contact__button">editar</button>
                        <button onClick={() => deleteContact(id)} className="container-contact__button">Borrar</button>
                    </>
            }
        </div>
    );
}

export default Contact;