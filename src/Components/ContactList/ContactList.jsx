import React, { useState, useEffect } from 'react'
import db from '../../Firebase/FirebaseConfig';
import Contact from '../Contact/Contact';

const ContactList = () => {
    const [contacts, setContacts] = useState([
        {id: 1, name: 'Jose', email: 'info@jose.com' },
        {id: 2, name: 'Miguel', email: 'miguel@jose.com' }
    ]);

    useEffect(() => {
        db.collection('users').limit(2).onSnapshot((snapshot) => {
            setContacts(snapshot.docs.map((document) => {
                return {...document.data(), id: document.id}
            }));
        });
    }, []);

    return (
        contacts.length > 0 && 
        <div className="container-contactList">
            {contacts.map((contact) => {
                return(
                    <Contact
                        key={contact.id}
                        id={contact.id}
                        name={contact.name}
                        email={contact.email}
                    />
                )
            })}
        </div>
    );
}
 
export default ContactList;