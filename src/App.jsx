import Form from './Components/Form/Form';
import ContactList from './Components/ContactList/ContactList';
import './App.scss';

function App() {
  return (
    <div className="App">
      <div className="app-container">
        <h2 className="app-container__title">Lista de contactos</h2>
        <Form />
        <ContactList />
      </div>
    </div>
  );
}

export default App;
